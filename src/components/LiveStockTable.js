import React from 'react';
import styled from 'styled-components';
import {dateFormatter} from '../utils/Formatter';

const Table = styled.table`
  width: 100%;
  border-spacing: 0px;
  border-collapse: collapse;
  margin-top: 30px;
  th,
  td {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 12px 8px;
  }
`;

const LiveStockTable = (props) => {
  const data = props.data;
  return (
    <Table>
      <thead>
        <tr>
          <th>StockId</th>
          <th>Date</th>
          <th>Number of animals</th>
          <th>Order of records</th>
          <th>Rating, Weight, Price</th>
        </tr>
      </thead>
      <tbody>
        {data && data.map((row, index) => {
          return (
            <tr key={index}>
              <td>{row.stockId}</td>
              <td>{dateFormatter(row.date)}</td>
              <td>{row.head}</td>
              <td>{row.index}</td>
              <td>{row.rating}, {row.weight}, ${row.price}</td>
            </tr>
          );
        })}
      </tbody>
    </Table>
  );
}

export default LiveStockTable
