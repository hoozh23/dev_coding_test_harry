## Coding Exercise

Build a small web and/or mobile app that displays the data of the attached csv file that satisfies the requirements below.

For the full-stack role, expose the data through an API which is then consumed by a front-end page. For a front-end role, read in the data directly from the app.

### Data Description

See the input.csv file of livestock records, which describes groups of livestock over time.

* stockId: Unique identifier of a group of animals
* date: The date the record is describing
* head: The number of animals
* index: Order of records when more than one fall on a day
* rating, weight price: Attributes of stock on that day

## Requirements:

1. As a grazier, I would like to see a list of monthly summaries for a stock item when I search by a stockId. Each summary should include:

    1.1. The month (In format MMM-YYYY. Eg: Jan-2021)

    1.2. The average rating for the month

    1.3. The final reading of head, weight, and price

    1.4. The number of updates (records) for the month

    1.5. The change in head from first record in the month to the last record in the month

## Business Rules:

1. Stock items are identified by stockId

2. Display the list of monthly summaries in tabular form

3. The summaries should be ordered by date, with the most recent one shown first.