import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { csv } from 'd3';
import CSVFile from './input.csv';
import LiveStockTable from './components/LiveStockTable';
import { getAverageRating } from './utils/Formatter';

const PageContainer = styled.div`
  max-width: 1200px;
  padding: 24px;
  margin: 0 auto;
`;

const Title = styled.h1`
  font-family: sans-serif;
  font-size: 32px;
  font-weight: 600;
  text-align: center;
`;

const App = () => {
  const [data, setData] = useState();
  const [query, setQuery] = useState("");

  useEffect(() => {
    csv(CSVFile).then(data => {
      setData(data);
    });
  }, []);


  function search(rows) {
    const searchedResponse = rows ? rows.filter(row => row.stockId.toString().indexOf(query) > -1) : rows;
    const response = searchedResponse
      ? searchedResponse.sort((a, b) => {
          return new Date(b.date) - new Date(a.date);
        })
      : searchedResponse;
    if (response) getAverageRating(response);
    return response;
  }



  return (
    <PageContainer>
      <Title>
        Livestock Summary Table
      </Title>
      <input type="text" value={query} placeholder="search" onChange={(e) => setQuery(e.target.value)} />

      <LiveStockTable data={search(data)} />
    </PageContainer>
  )
}

export default App
